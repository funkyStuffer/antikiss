
function getBase64Image(img) {
    // Create an empty canvas element
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;

    // Copy the image contents to the canvas
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);

    // Get the data-URL formatted image
    // Firefox supports PNG and JPEG. You could check img.src to
    // guess the original format, but be aware the using "image/jpg"
    // will re-encode the image.
    var dataURL = canvas.toDataURL("image/png");

    return md5(dataURL.replace(/^data:image\/(png|jpg);base64,/, ""));
}

function clickImg(element){
    if(window.localStorage.getItem(element) === null){
        console.log("nop");
        return element;
    }

    var src = window.localStorage.getItem(element),
    done = false;
    $("#formVerify img").each(function(index, el) {
        var base64 = getBase64Image(this);
        console.log(this);
        if (src === base64){
            this.id = element;
            $(this).click();
            done = true;
            return false;
        }

    });
    if(done)
        return element;
    else
        return clickImg(element + "#");
        
}


function storeDb() {
    var values = [],
        keys = Object.keys(localStorage),
            i = keys.length;

    while ( i-- ) {
        if(localStorage.getItem(keys[i]).length === 32)
            values.push(localStorage.getItem(keys[i]));
    }
    $.getJSON('http://antikiss.epizy.com/connection.php?callback=?', function(res){
            var keys = Object.keys(res),
            j = keys.length;

            if(Object.keys(res).length > values.length){
                while(j--)
                    localStorage.setItem(keys[j], res[keys[j]]);
        }
    });
}










if($("div").length === 0){
    var $_GET = {};
    if(document.location.toString().indexOf('?') !== -1) {
        var query = document.location
                       .toString()
                       .replace(/^.*?\?/, '')
                       .replace(/#.*$/, '')
                       .split('&');
    
        for(var i=0, l=query.length; i<l; i++) {
           var aux = decodeURIComponent(query[i]).split('=');
           $_GET[aux[0]] = aux[1];
        }
    }
    if(localStorage.getItem("check data") === "on"){
        localStorage.removeItem($_GET['el1']);
        localStorage.removeItem($_GET['el2']);
        localStorage.removeItem("check data");
    }
    $("a:contains('HERE')").get(0).click();
}

if($("#formVerify").length === 1){
    storeDb();
    $($("#formVerify span").get(0)).attr("id", "el1");
    $($("#formVerify span").get(1)).attr("id", "el2");
    $("#formVerify").attr('action', $("#formVerify").attr('action') + "?")
    var firstElement = {desc: null, src: null},
    element1 = $('#el1').text().trim(),
    element2 = $('#el2').text().trim();


    $("#el2").hide();


    $("#formVerify img").on('click', function(event) {

        if(firstElement.src === null){
            firstElement = {desc: this.id.length > 0?this.id:element1, src: $(this)};
            console.log("2: "+element2);
            element2 = clickImg(element2);
            if(window.localStorage.getItem(element2) === null){
                $("#formVerify").attr('action', $("#formVerify").attr('action') + "el2=" + element2);
                localStorage.setItem("check data", "on");
            }
            
            $("#el1").fadeOut(250, function() {
                $("#el2").fadeIn(250);
            });
        }
        else {
            if($(this).attr('src') === firstElement.src.attr('src')){
                firstElement = {desc: null, src: null};
                $("#el2").fadeOut(250, function() {
                    $("#el1").fadeIn(250);
                });
                return;
            }
            console.log(element1);
            window.localStorage.setItem(firstElement.desc, getBase64Image(firstElement.src.get(0)));
            window.localStorage.setItem(this.id.length > 0?this.id:element2, getBase64Image(this));
            $("#el1").fadeIn(500);
        }
    });

    if(window.localStorage.getItem(element1) !== null){
        console.log("1: "+element1);
        element1 = clickImg(element1);
        if(window.localStorage.getItem(element1) !== null){
            $("#el2").hide();
            $("#el1").show();
        }else{
            $("#formVerify").attr('action', $("#formVerify").attr('action') + "el1=" + element1 + "&");
            localStorage.setItem("check data", "on");
        }
    }

}






if($("#divContentVideo").length !== 0){
    var index1 = $("#selectEpisode").attr('onchange', 'window.location = this.value').parent().clone(),
    index21 = $("#btnPrevious").parent().clone(),
    index22 = $("#btnNext").parent().clone(),
    controls = $("#selectServer").parent().clone(),
    video = $("video").clone();

    index1.append(index21).append(index22).removeAttr('style').css('text-align', 'center');

    video.prop("muted", false)
        .prop("controls", true);
    var preload = video.clone().prop("muted", true);
    preload.css("display", "none");
    var quality = $("<div />");
    quality.append($("a:contains('360p')"))
            .append($("a:contains('480p')"))
            .append($("a:contains('720p')"));

    vid = $("#divContentVideo").css('width', '930px').css('text-align', 'center').css('margin-bottom', '50px')
                .clone().prepend(controls).prepend(index1);
    $("body").html("").css('position', 'relative').append(vid).append(vid).append(quality)
    .append($("<a class='bigChar'>Home</a>").attr('href', "//"+location.host).css('margin-right', '50px'));
    $("iframe").css('width', '100%').css('text-align', 'center');
    $("#switch").remove();

    $.ajax({
        dataType: 'html',
        type: 'get',
        url: location.href.substring(0, location.href.lastIndexOf("/")),
        success: function (responseData) {
            $("body").append($(responseData).find(".bigChar")).prepend($(responseData).find(".listing").width(350));
            $(".listing").wrap('<div class="wrapper"></div>');
            $(".wrapper").css({
                position: 'absolute',
                right: '0',
                top: '40px',
                'overflow-x': 'hidden',
                'text-align': 'left',
                'max-height': '535px',
                width: 'calc(100% - 960px)'
            });
        },
        error: function (responseData) {
            console.error("couldnt get eps");
        }
    });

}else $("html").append("<style>*[id^='BB'], iframe{display:none;}</style>");

var checking = false;

function checkVid(i=5) {
    var iframe=$("iframe");
    checking = true;
    try{
        if(iframe.contents().find('#divContentVideo').length !== 0){
            // console.log("ui");
            location = iframe.get(0).src;
        }
        else{
            // console.log("non");
            setTimeout(checkVid, 200);
        }
    }catch(err){
        if(i<0)
            $("a").click();
        
        setTimeout("checkVid("+(--i)+")", 200);
    }
}

$("a").on('click', function(event) {
    if(this.href.indexOf("/Anime/") !== -1 &&
        this.href.lastIndexOf("/") - (this.href.indexOf("/Anime/") + 7) > 0){
        event.preventDefault();
        var src = $(this).attr('href'),
        el = this, i=1;

        $("body").html("<span id='trial'>loading...</span><a style='cursor:pointer;' href='"+src+"'>proceed with captcha?</a>")
            .append($('<iframe />').attr('src', src).attr('display', 'none')
            .on('load', function(e) {
                if(!checking)
                    checkVid();
                if($("iframe").contents().find('#divContentVideo').length === 0){
                    $("#trial").text("loading (try "+(i++)+")...");
                    $("iframe").attr('src', '').attr('src', src);
                }
            }));
    }
});